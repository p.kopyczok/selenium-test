import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_1_Correct_Registration extends SeleniumBaseTest {


    @Test
    public void correctRegistrationTest() {
        String randomEmail = UUID.randomUUID().toString().substring(0, 5) + "@gmail.com";

        new LoginPage(driver)
                .goToRegistration()
                .typeEmail(randomEmail)
                .typePassword(config.getApplicationPassword())
                .typeConfirmPassword(config.getApplicationPassword())
                .submitRegister()
                .assertWelcomeElementIsShown();
    }
}
