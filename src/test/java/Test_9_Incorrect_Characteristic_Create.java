import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_9_Incorrect_Characteristic_Create extends SeleniumBaseTest {

    @Test
    public void IncorrectAddCharacteristicTest() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 5);
        String lsl = "8";

        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .submitCreateWithFailure()
                .assertProcessError("The value '' is invalid.")
                .backToList()
                .assertCharacteristicIsNotShown(characteristicName);
    }
}
