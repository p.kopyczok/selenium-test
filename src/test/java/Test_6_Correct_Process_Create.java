import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_6_Correct_Process_Create extends SeleniumBaseTest {

    @Test
    public void AddProcessTest() {
        String processName = UUID.randomUUID().toString().substring(0, 5);
        String processDescription = UUID.randomUUID().toString().substring(0, 10);
        String processNote = UUID.randomUUID().toString().substring(0, 10);

        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .typeDescription(processDescription)
                .typeNote(processNote)
                .submitCreate()
                .assertProcess(processName, processDescription, processNote)
                .goToDashboard()
                .assertDashboardProcessPresence(processName);
    }
}
