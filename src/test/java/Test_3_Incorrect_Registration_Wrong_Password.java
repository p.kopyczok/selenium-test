import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_3_Incorrect_Registration_Wrong_Password extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] wrongPasswords() {
        return new Object[][]{
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test12", "Passwords must have at least one non alphanumeric character."},
                {"Test!!", "Passwords must have at least one digit ('0'-'9')."}
        };
    }

    @Test(dataProvider = "wrongPasswords")
    public void incorrectRegistrationTestWrongPassword(String password, String expErrorMessage) {
        String randomEmail = UUID.randomUUID().toString().substring(0, 5) + "@gmail.com";

        new LoginPage(driver)
                .goToRegistration()
                .typeEmail(randomEmail)
                .typePassword(password)
                .typeConfirmPassword(password)
                .submitRegisterWithFailure()
                .assertPasswordErrorAgainstRulesIsShown(expErrorMessage);
    }
}
