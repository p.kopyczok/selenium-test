import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_10_Report extends SeleniumBaseTest {

    @Test
    public void ReportTest() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";
        String sampleName = "Test sample";
        String results = "8.0;9.0";
        String expMean = "8.5000";

        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeUsl(usl)
                .typeLsl(lsl)
                .submitCreate()
                .gotToResults(characteristicName)
                .clickAddResultsSample()
                .typeSampleName(sampleName)
                .typeResults(results)
                .submitCreate()
                .backToCharacteristics()
                .goToReport(characteristicName)
                .assertMean(expMean);
    }
}
