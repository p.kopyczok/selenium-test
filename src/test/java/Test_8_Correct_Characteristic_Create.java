import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_8_Correct_Characteristic_Create extends SeleniumBaseTest {

    @Test
    public void addCharacteristicPage() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 5);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeUsl(usl)
                .typeLsl(lsl)
                .submitCreate()
                .assertCharacteristic(characteristicName, lsl, usl, "")
                .goToDashboard()
                .assertCharacteristicsDashboardPresence(characteristicName);
    }
}
