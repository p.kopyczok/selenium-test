import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_4_Correct_Login extends SeleniumBaseTest {

    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown();
    }
}
