import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_5_HomePage_Menu_Test extends SeleniumBaseTest {

    @Test
    public void homePageMenuTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessesUrl("http://localhost:4444/Projects")
                .goToCharacteristics()
                .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                .goToDashboard()
                .assertDashboardUrl("http://localhost:4444/");
    }
}
