import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_2_Incorrect_Registration_Different_Passwords extends SeleniumBaseTest {

    @Test
    public void incorrectRegistrationTestDifferentPassword() {
        String randomEmail = UUID.randomUUID().toString().substring(0, 5) + "@gmail.com";

        new LoginPage(driver)
                .goToRegistration()
                .typeEmail(randomEmail)
                .typePassword("Test1!")
                .typeConfirmPassword("Test12")
                .submitRegisterWithFailure()
                .submitRegisterWithFailure()
                .assertPasswordErrorIsShown();
    }
}
