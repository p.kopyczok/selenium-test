import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_7_Incorrect_Process_Create extends SeleniumBaseTest {


    @Test
    public void IncorrectAddProcessTest() {
        String shortProcessName = "ab";

        new LoginPage(driver)
                .typeEmail(config.getApplicationEmail())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(shortProcessName)
                .submitIncorrectProcessCreate()
                .assertProcessNameError()
                .backToList()
                .assertProcessIsNotShown(shortProcessName);
    }
}
