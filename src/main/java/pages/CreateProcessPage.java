package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessPage extends HomePage {

    public CreateProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Name")
    private WebElement processNameTxt;

    @FindBy(id = "Description")
    private WebElement processDescriptionTxt;

    @FindBy(id = "Notes")
    private WebElement processNoteTxt;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error")
    private WebElement nameError;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBnt;

    public CreateProcessPage typeName(String processName) {
        processNameTxt.clear();
        processNameTxt.sendKeys(processName);
        return this;
    }

    public CreateProcessPage typeDescription(String description) {
        processDescriptionTxt.clear();
        processDescriptionTxt.sendKeys(description);
        return this;
    }

    public CreateProcessPage typeNote(String note) {
        processNoteTxt.clear();
        processNoteTxt.sendKeys(note);
        return this;
    }

    public ProcessesPage submitCreate() {
        createBtn.click();
        return new ProcessesPage(driver);
    }

    public ProcessesPage backToList(){
        backToListBnt.click();
        return new ProcessesPage(driver);
    }

    public CreateProcessPage submitIncorrectProcessCreate(){
        createBtn.click();
        return this;
    }
    public CreateProcessPage assertProcessNameError(){
        Assert.assertTrue(nameError.getText().contains("The field Name must be a string with a minimum length of 3 and" +
                " a maximum length of 30."));
        return this;
    }
}
