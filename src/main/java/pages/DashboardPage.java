package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    private String GENERIC_PROCESS_ROW_XPATH = "//h2[text()='%s']";
    private String GENERIC_CHARACTERISTICS_TABLE_XPATH = "//p[contains(text(), '%s')]";

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public DashboardPage assertDashboardUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }

    public DashboardPage assertDashboardProcessPresence(String expName) {
        String dashboardXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);
        driver.findElement(By.xpath(dashboardXpath));
        return this;
    }

    public DashboardPage assertCharacteristicsDashboardPresence(String expName) {
        String dashboardXpath = String.format(GENERIC_CHARACTERISTICS_TABLE_XPATH, expName);
        driver.findElement(By.xpath(dashboardXpath));
        return this;
    }
}