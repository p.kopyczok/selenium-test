package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;


public class RegisterPage {

    protected WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement RegisterBtn;

    @FindBy(css = ".validation-summary-errors")
    private List<WebElement> passwordError;

    public RegisterPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public RegisterPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public RegisterPage typeConfirmPassword(String confirmPassword) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(confirmPassword);
        return this;
    }

    public HomePage submitRegister() {
        RegisterBtn.click();
        return new HomePage(driver);
    }

    public RegisterPage submitRegisterWithFailure() {
        RegisterBtn.click();
        return this;
    }

    public RegisterPage assertPasswordErrorIsShown() {
        Assert.assertEquals(passwordError.get(0).getText(), "The password and confirmation password do not match.");
        return this;
    }

    public RegisterPage assertPasswordErrorAgainstRulesIsShown(String expError) {
        Assert.assertEquals(passwordError.get(0).getText(), expError);
        return this;
    }
}
