package config;

import java.io.InputStream;
import java.util.Properties;

public class Config {
    private Properties properties;

    private Properties getProperties() {
        Properties prop = new Properties();
        try {
            InputStream inputStrem = getClass().getClassLoader().getResourceAsStream("config.properties");
            prop.load(inputStrem);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load properties file: " + e);
        }
        return prop;
    }

    public Config() {
        properties = getProperties();
    }

    public String getApplicationUrl() {
        return properties.getProperty("application.url");
    }

    public String getApplicationUser() {
        return properties.getProperty("application.user");
    }

    public String getApplicationPassword() {
        return properties.getProperty("application.password");
    }

    public String getApplicationEmail() {
        return properties.getProperty("application.email");
    }
}